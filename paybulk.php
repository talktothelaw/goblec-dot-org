<?php
session_start();
require_once "db.php";
require_once "function.php";
require_once "config.php";

$brref = isset($_REQUEST['bulkid']) ? mysqli_real_escape_string($conn,$_REQUEST['bulkid']) : "";
$q = "SELECT * FROM bulkreg WHERE BRRef = '$brref'";
//die($q);
$bdata = mysqli_query($conn,$q ) or die(mysqli_error($conn));
if(mysqli_num_rows($bdata)<1){
    die("INVALID REFERENCE NUMBER $brref");
}
$bd = mysqli_fetch_assoc($bdata);
//die(json_encode($bd));

?>
<link rel="stylesheet" href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>
<link rel="stylesheet" href='https://use.fontawesome.com/releases/v5.7.2/css/all.css'>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<style>
    @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');

body {
   background-image: url('images/pbg.jpg');
    background-size: cover;
    font-family: Times New Roman;
}

.card {
    width: 500px;
    border: none;
    border-radius: 10px;
    background: green;
}

p.top {
    font-size: 17px
}

.discount {
    background-color: #1BC5DF;
    border: none;
    border-top-left-radius: 25px;
    border-bottom-left-radius: 25px;
    padding: 5px 15px;
    transform: translateX(24px);
    height: 35px
}

.discount span {
    font-size: 15px
}

h2 {
    letter-spacing: 2px
}

.fa-euro-sign {
    font-size: 27px;
    color: #B3C4FA
}

.card-content p {
    line-height: 18px;
    font-size: 20px;
    color: #abbef6
}

.btn-primary {
    border: none;
    border-radius: 6px;
    background-color: #647EDF;
    padding-top: 0;
    height: 46px
}

.btn-primary span {
    font-size: 13px;
    color: #D1E2FF;
    margin-right: 10px
}

.fa-arrow-right {
    font-size: 12px;
    color: #D1E2FF
}

.btn-primary:hover,
.btn-primary:focus {
    background-color: #647EDF;
    box-shadow: none
}
</style>


<div class="container d-flex justify-content-center">
    <div class="card mt-5 p-4 text-white">
        <p class="top mb-1">You need to pay</p>
        <div class="d-flex flex-row justify-content-between text-align-center xyz">
            <h2>&#8358;<span><?php echo number_format($bd['BRAmount']); ?></span></h2>
            <div class="discount"><span>Live</span></div>
        </div>
        
        
        <form id="paymentForm">
    <input type="hidden" id="email-address" value="<?php echo $bd['BRPEmail']; ?>" required />
    <input type="hidden" id="amount" value="<?php echo $bd['BRAmount']; ?>" required />
    <input type="hidden" value="<?php echo$bd['BRPayer']; ?>" id="first-name" />
    <input type="hidden" id="last-name" value=""/>
    

        
        
        <div class="card-content mt-4">
            <p>Once you complete the payment, you can start enjoying all our features.</p>
        </div>
        <div class="mt-2"> <button type="submit" class="btn btn-block btn-lg btn-primary" onclick="payWithPaystack()"><span>Make payment</span><i class="fas fa-arrow-right"></i></button> </div>
    
    </form>
    
    </div>
</div>



<script src="https://js.paystack.co/v1/inline.js"></script>
<script>

    const paymentForm = document.getElementById('paymentForm');
    paymentForm.addEventListener("submit", payWithPaystack, false);
    function payWithPaystack(e) {
        e.preventDefault();
        let handler = PaystackPop.setup({
            //key: 'pk_test_023c3fedabf7706b0c2dce837f5601d453735810', // 'pk_live_5a4a99377b3ba8117c41baeb5ae9a82d3ad45ba1',
            key: 'pk_live_5a4a99377b3ba8117c41baeb5ae9a82d3ad45ba1', // 'pk_live_5a4a99377b3ba8117c41baeb5ae9a82d3ad45ba1',
            
            email: document.getElementById("email-address").value,
            amount: document.getElementById("amount").value * 100,
            ref: '<?= $bd['BRRef']; ?>',
            // label: "Optional string that replaces customer email"
            onClose: function(){
                alert('Window closed.');
            },
            callback: function(response){
                $.ajax({
                    url: "./databulk.php?ref="+response.reference,
                    type: "post",
                    // data: postData,
                    // dataType: "JSON",
                    success: function (data) {
                        let message = 'Payment complete! Reference: ' + response.reference + 'Your beneficiaries can now login to update their passports';
                alert(message);
                         setTimeout(function(){
                              location.href="thank-you.php";
                        },3000);
                    }
                });
                

            }
        });
        handler.openIframe();
    }

</script>


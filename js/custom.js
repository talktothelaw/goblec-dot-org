"use strict";

//$(".pwstrength").pwstrength();

$(".btn-move").click(function(){

	var fname = $("#firstname").val();
	var lname = $("#lastname").val();
	var mail = $("#email").val();
	var addr = $("#address").val();
	var phone = $("#phone").val();
	var state = $("#state").val();
	var train = $("#training").val();
	var unit = $("#unit").val();
	var ngo = $("#ngo").val();

var itemerror = false;
let items = document.getElementsByClassName('materials');
for(let i = 0; i < items.length; i++){
    if(items[i].checked == false){
        var itemerror = true;
    }
}


	if(itemerror){
	    iziToast.error({
    title: 'Error!',
    message: 'All training items are required',
    position: 'topRight'
  });
	}else if(fname === ""){

	iziToast.error({
    title: 'Error!',
    message: 'Your first name is empty',
    position: 'topRight'
  });

	}else if(lname ===""){

	iziToast.error({
    title: 'Error!',
    message: 'Your last name is empty',
    position: 'topRight'
  });

	}else if(mail ===""){

	iziToast.error({
    title: 'Error!',
    message: 'Please enter your email',
    position: 'topRight'
  });

	}else if(addr ===""){

	iziToast.error({
    title: 'Error!',
    message: 'You must enter your address',
    position: 'topRight'
  });

	}else if(phone ===""){

	iziToast.error({
    title: 'Error!',
    message: 'Your phone number is needed, we may conatct you',
    position: 'topRight'
  });

	}else if(state ===""){
	iziToast.error({
    title: 'Error!',
    message: 'Please select your state',
    position: 'topRight'
  });

  }else if(unit ===""){
	iziToast.error({
    title: 'Error!',
    message: 'Please select your unit',
    position: 'topRight'
  });
	}else if(train ===""){
	iziToast.error({
    title: 'Error!',
    message: 'Please select your training',
    position: 'topRight'
  });

	}else{

	if ($("input[name='agree']:checked").val()){
  // it is checked

		var _tdata = {
			"total" : $("#priceplace").text(),
			"materials" : training_materials
		};


		//calculate training items and price

		//end of calculating training items and price

  $.ajax({
	url:'reservation.php?l',
	method:'post',
	dataType:'html',
	data:{
		fname:fname, lname:lname, mail:mail, addr:addr, phone:phone, state:state, train:train, unit:unit,_tdata:_tdata,ngo:ngo
	},
	success:function(data){
			//console.log(data);
			if(data==='empty'){
				iziToast.error({
			title: 'Error!',
			message: 'Please complete the form input',
			position: 'topRight'
		  });

			}else if(data==='invalid_email'){
				iziToast.error({
			title: 'Error!',
			message: 'Please enter a valid email address',
			position: 'topRight'
		  });

			}else if(data==='email_exist'){
				iziToast.error({
			title: 'Error!',
			message: 'Email address has already been used',
			position: 'topRight'
		  });

			}else if(data==='phone_exist'){
				iziToast.error({
			title: 'Error!',
			message: 'Phone number has already been used',
			position: 'topRight'
		  });

			}else if(data==='manipulation'){
				iziToast.error({
			title: 'Error!',
			message: 'Unable to Process reservation, Please Refresh your page and try again',
			position: 'topRight'
		  });

			}else if(data==='unable'){
				iziToast.error({
			title: 'Error!',
			message: 'Oops, we are unable to process this request, check your network and try again',
			position: 'topRight'
		  });

			}else if(data ==='succ'){

			iziToast.success({
			title: 'Success!',
			message: 'Account Created successfully',
			position: 'topRight'
		  });

	setTimeout(function(){
	  window.location.assign("payment.php");
  }, 3000);
	}


	}

  });

}else{

iziToast.error({
    title: 'Error!',
    message: 'You must agree with our terms and condition',
    position: 'topRight'
  });
}

	}

});

$("#continueReservation").click(function(){
	var phoneid = $("#myphone").val();

	if(phoneid ===''){
		iziToast.warning({
		title: 'Info!',
		message: 'Please enter your phone number',
		position: 'topRight'
	  });
	}else{

	$.ajax({
	url:'continue_reservation.php',
	method:'post',
	dataType:'json',
	data:{
		phoneid:phoneid
	},
	success:function(phone){
		console.log(phone);
		if(phone.status ==='empty'){
			iziToast.error({
			title: 'Error!',
			message: ''+phone.resp+'',
			position: 'topRight'
		  });
		}else if(phone.status ==='not'){
			iziToast.error({
			title: 'Error!',
			message: ''+phone.resp+'',
			position: 'topRight'
		  });

		  setTimeout(function(){
			location.reload();
			}, 4000);
		}else if(phone.status ==='completed'){
			iziToast.success({
			title: 'Good Job!',
			message: ''+phone.resp+'',
			position: 'topRight'
		  });
		  setTimeout(function(){
			window.location.assign("thank-you.php?4839GH(839d0-sd9-03-023ue98e9839h98d98dsd98");
			}, 4000);
		}else if(phone.status ==='payment'){
			iziToast.success({
			title: 'Found!',
			message: ''+phone.resp+'',
			position: 'topRight'
		  });
			setTimeout(function(){
			window.location.assign("payment.php");
			}, 3000);
		}
	}

	});
}
});

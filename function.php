<?php
function send_mail($send_to, $message, $subject, $company_name, $company_email, $company_domain, $email_password){
        require_once('mailer/class.phpmailer.php');
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "";
        $mail->Host = $company_domain;
        $mail->Port = 587;
        $mail->AddAddress($send_to);
        $mail->Username = $company_email;
        $mail->Password = $email_password;
        $mail->SetFrom($company_email, $company_name);
        $mail->AddReplyTo($company_email, $company_name);
        $mail->Subject = $subject;
		//$mail->AddEmbeddedImage('logo.jpg', 'logo.png', 'logo.jpeg', 'logo.gif');
		//$mail->addAttachment("file.txt", "File.txt");        
		//$mail->addAttachment("images/profile.png"); //Filename is optional
        $mail->MsgHTML($message);
        $mail->Send();
    }

function ref_id($length){
    $characters = "4XZCCXC83GJASASDd974ASA938DSD938F928H94SB9dSDDSDSd30X92SDSD09029HF323DKS749849K049340D30SFDF1M089129F0NMSDS11BNM010SD1A8SDSBVBSVDD30SD237SD8233D100VBVBSD1FDF30G2394ZCVCB9834J8734";
    $charactersLength = strlen($characters);
    $randomString = "";
    for($i = 0; $i < $length; $i++ ){
        $randomString .=$characters[rand(0, $charactersLength -1)];
    }
    return $randomString;
}

function get_ip(){ 
if(isset($_SERVER['HTTP_CLIENT_IP'])){
	return $_SERVER['HTTP_CLIENT_IP'];
}elseif(isset($_SERVER['HTTP_x_FORWARDED_FOR'])){
	return $_SERVER['HTTP_x_FORWARDED_FOR'];
}
else{
	return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']: '');	
}
}

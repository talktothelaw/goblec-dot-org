<?php
session_start();
require_once "db.php";
require_once "config.php";
if(isset($_SESSION['completed'])){
$myid = $_SESSION['completed'];
$query = mysqli_query($conn, "select * from ".user." LEFT JOIN goblecngo ON NId = PNGO where PId = '$myid'") or die(mysqli_error($conn));
$result = mysqli_fetch_assoc($query);
//uploadpassport/user.png
?>
<link rel="stylesheet" href='css/bootstrap.min.css'>
<link rel="stylesheet" href='font-awesome/css/font-awesome.css'>
<link rel="stylesheet" href='css/iziToast.min.css'>
<style>
body {
  font-family: 'Arial';
  background: url('images/pbg.jpg');
  background-size:cover;
}

a {
  color: #c20;
  text-decoration: underline;
}

.centered {
  margin: 0 auto;
}

.finer-print {
  font-size: 15px;
}

.span2 {
  width: 35%;
  padding: 0;
  float: left;
}

.business-card {
  position: relative;
  background: #eeeeec;
  color: #2e3436;
  width: 400px;
  height: 282px;
  font-size: 20px;
  border-bottom: 2px solid #d3d7df;
  border-radius: 15px;
  box-shadow: 0 0 10px 1px #000;
  margin-top: 5%;
}

.business-card .title {
    height: 57px;
  background: red;
  color: #fff;
  padding: 10px;
  font-weight: bold;
  font-size: 20px;
  border-top: 2px solid red;
  border-left: 2px solid red;
  border-radius: 14px 14px 0 0;
}

.business-card .content {
  font-weight: bold;
  padding-left: 10px;
}

.business-card .content2 {
  font-weight: bold;
  padding-left: 5px;
  padding-right: 25px;
}

.business-card .avatar {
  float: right;
  max-width: 100px;
  max-height: 100px;
  box-shadow: 0 0 10px 1px #777;
  border-radius: 3px;
}

.business-card .footer {
  position: absolute;
  bottom: 10px;
  left: 2%;
  font-size: 15px;
  padding-top: 5px;
  border-top: 1px solid;
  width: 96%;
}
.QRcode {
  text-align: right;
  margin: 10px 18px 0 0;
}
.QRcode2 {
  text-align: right;
  margin: 10px 18px 0 0;
}

.hiddenFileInput > input{
  height: 100%;
  width: 100;
  opacity: 0;
  cursor: pointer;
}
.hiddenFileInput{
  border: 1px solid #ccc;
  width: 100px;
  height: 100px;
  display: inline-block;
  overflow: hidden;
  cursor: pointer;
  
  /*for the background, optional*/
  background: center center no-repeat;
  background-size: 75% 75%;
  background-image:  url('images/add-image.jpg');
}
</style>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	
	<body>
	
	<?php
   if($result['imgPassport'] ==null){
   ?>
<div class="business-card centered">
   
   
  <div class="QRcode2">
		<div class="container">
			<div class="row justify-content-center">
			
			<div class="content span2">
				<img id="preview" style="width:100px; height:100px" src="uploadpassport/user.png" alt="avatar" class="avatar" />
				<br>

					<form method="post" action="auto_capture.php" enctype="multipart/form-data">
						<div class="form-group">
							<input type="file" name="fileToUpload" id="fileToUpload" class="form-control hiddenFileInput" accept="image/*" capture="on">
						</div>
						
						<div class="form-group">
							<button class="btn btn-primary form-control" name="submit" type="submit" id="submit">Upload</button>
						</div>
					</form>
			  </div>	
			</div>
		</div>
    </div>
</div>

<?php
   }else{
   ?>
   
   
<div class="business-card centered">
  <div class="title" >
    <img src="images/logo.png" />
    <?= $result['NGO']; ?>
  </div>
  <br>
  <div class="content span2" style="text-transform:uppercase;font-size:small;">
      Unit <?= $result['PUnitId']; ?>
      <br/>
      <?php echo $result['PFirstname'].' ',$result['PLastname']; ?><br />
    <span class="finer-print"><?php echo $result['PPhone']; ?><br />
      <?php echo $result['PState']; ?>
    </span>
	<br>
	<br>
	<div class="finer-print shadow" style="font-size:18;font-family:Eras Bold ITC">
      <i class="fa fa-certificate text-danger text-center"></i>Reservation: <?php echo $result['PReservationCode']; ?><br />
    </div>
  </div>
  <div class="content2 span3">
    <img style="float:right" src="<?php echo $result['imgPassport']; ?>" alt="avatar" class="avatar" />
  </div>
  <br>
  <br>
  <br>
  <span style="font-size: x-small;
    font-weight: bold;
    color: #dc3545;">
      <br/>
      </span>
  <br>
  <br>
  <div class="QRcode" style="width: 200px;
    float: right;
    position: relative;
    bottom: 53px;">
        <img src='https://chart.googleapis.com/chart?cht=qr&chl=fastenal.com&chs=50x50&choe=UTF-8&chld=L|2' alt=''> 
		<br><?php echo substr($result['PRegisteredDate'], 0, 10); ?>
      </div>
	  <?php //if(isset($_SESSION['msg'])){ echo $_SESSION['msg']; unset($_SESSION['msg']);} ?>
</div>
<?php
   }
   ?>
   </body>
<script src='js/jquery.min.js'></script>
<script src='js/bootstrap.bundle.min.js'></script>
<script src='js/iziToast.min.js'></script>
<script src='js/custom.js'></script>
<script>

$("input[type=file]").on("change", function() {
  $("[for=file]").html(this.files[0].name);
  $("#preview").attr("src", URL.createObjectURL(this.files[0]));
})
</script>

<?php
if(isset($_SESSION['image'])){
	?>
	<script>
		iziToast.<?php echo $_SESSION['type']; ?>({
		title: '<?php echo $_SESSION["title"]; ?>',
		message: '<?php echo $_SESSION["message"]; ?>',
		position: 'topRight'
	  });
	  </script>
	<?php
	unset($_SESSION['image']);
	unset($_SESSION['type']);
	unset($_SESSION['title']);
	unset($_SESSION['message']);
}
?>

<?php
}else{
	header('location: index.php');
}
?>
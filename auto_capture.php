<?php
session_start();
require_once "db.php";
require_once "config.php";

if(isset($_SESSION['completed'])){
$myid = $_SESSION['completed'];
if(isset($_POST["submit"])) {
	
	$target_dir = "uploadpassport/";
	$target_file = $target_dir .time() . '-' . basename($_FILES["fileToUpload"]["name"]);
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		
  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
  if($check !== false) {
	
// Check if file already exists
if (file_exists($target_file)) {
	$_SESSION['type'] = "error";
	  $_SESSION['message'] = "Sorry, you have already uploaded passport.";
	  $_SESSION['title'] = "Error!";
	  $_SESSION['image'] = true;
	header("location: capture.php");
}else{

// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000) {
	$_SESSION['type'] = "error";
	  $_SESSION['message'] = "Sorry, your file is too large.";
	  $_SESSION['title'] = "Error!";
	  $_SESSION['image'] = true;
	header("location: capture.php");
}else{

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
	$_SESSION['type'] = "error";
	  $_SESSION['message'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	  $_SESSION['title'] = "Error!";
	  $_SESSION['image'] = true;
	header("location: capture.php");
}else{
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

	  
	   $sql = mysqli_query($conn, "UPDATE ".user." SET imgPassport = '$target_file' where PId = '$myid'") or die(mysqli_error($conn));
	  
	  $_SESSION['type'] = "success";
	  $_SESSION['message'] = "Profile image has been uploaded";
	  $_SESSION['title'] = "Success!";
	  $_SESSION['image'] = true;
	header("location: capture.php");
  } else {
	  $_SESSION['type'] = "error";
	  $_SESSION['message'] = "Sorry, there was an error uploading your passport.";
	  $_SESSION['title'] = "Error!";
	  $_SESSION['image'] = true;
	header("location: capture.php");
  }


}
  }
  
}


} else {
	$_SESSION['type'] = "error";
	  $_SESSION['message'] = "Sorry this file is not an image.";
	  $_SESSION['title'] = "Error!";
	  $_SESSION['image'] = true;
	header("location: capture.php");
  }


}else{
	header("location: capture.php");
}
}else{
	echo "invalid Session";
	header("location: index.php");
}
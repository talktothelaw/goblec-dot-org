<?php
session_start();
require_once "db.php";
require_once "function.php";
require_once "config.php";
if(isset($_POST['experience']) AND isset($_POST['experience_message'])){
	
	if(empty($_POST['experience']) AND empty($_POST['experience_message'])){
		//empty
		$_SESSION['msg'] = "<div class='alert alert-danger'>You need to enter your message before sending!</div>";
			header("location: index.php");
	}else{
		
		$send_to = mysqli_real_escape_string($conn, $_POST['experience']);
		$message = mysqli_real_escape_string($conn, $_POST['experience_message']);
		$subject = mysqli_real_escape_string($conn, $_POST['subject']);
		
		if(send_mail($send_to, $message, $subject, $company_name, $company_email, $company_domain, $email_password)){
			$_SESSION['msg'] = "<div class='alert alert-success'>Thank you, your message has been received  successful, we value your feedback</div>";
			header("location: index.php");
		}else{
			$_SESSION['msg'] = "<div class='alert alert-danger'>Oops, something went wrong, we couldn't deliver your message</div>";
			header("location: index.php");
		}
		

	}
}else{
	$_SESSION['msg'] = "<div class='alert alert-danger'>Session Expired</div>";
			header("location: index.php");
}
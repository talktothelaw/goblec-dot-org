<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>TRAINING RESERVATION</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<link rel="stylesheet" type="text/css" href="css/opensans-font.css">
	<link rel="stylesheet" type="text/css" href="fonts/line-awesome/css/line-awesome.min.css">
	<!-- Jquery -->
	<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
	<!-- Main Style Css -->
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/iziToast.min.css"/>
	
</head>
<body class="form-v4">
	<div class="page-content">
		<div class="form-v4-content">
			<div class="form-left">
				<h2>RESERVATION</h2>
				<h3>Terms & Conditions</h3>
				<p class="text-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et molestie ac feugiat sed. Diam volutpat commodo.</p>
				<hr style="color:white">
				<p class="text-2"><span>Eu ultrices:</span> Vitae auctor eu augue ut. Malesuada nunc vel risus commodo viverra. Praesent elementum facilisis leo vel.</p>
				<div class="form-left-last">
					<input type="submit" name="account" class="account" value="Continue Reservation">
				</div>
			</div>
			<form class="form-detail" action="#" method="post" id="myform">
				<h2 style="font-size:17px">TRAINING RESERVATION <br><small>Registration Form</small></h2>
				<div class="form-group">
					<div class="form-row form-row-1">
						<label for="first_name">First Name</label>
						<input type="text" name="first_name" id="first_name" class="input-text" required>
					</div>
					<div class="form-row form-row-1">
						<label for="last_name">Last Name</label>
						<input type="text" name="last_name" id="last_name" class="input-text" required>
					</div>
				</div>
				<div class="form-row">
					<label for="your_email">Your Email</label>
					<input type="text" name="your_email" id="your_email" class="input-text" required pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}">
				</div>
				<div class="form-row form-row-1">
						<label for="your_address">Your Address</label>
						<input type="text" name="your_address" id="your_address" class="input-text" required>
					</div>
				<div class="form-row">
					<label for="your_phone">Your Phone</label>
					<input type="text" name="your_phone" id="your_phone" class="input-text" required pattern="">
				</div>
				
					<div class="form-row form-row-1 ">
						<label for="your_state">State</label>
						<input type="text" name="your_state" id="your_state" class="input-text" required>
					</div>
					
					
					<div class="form-row form-row-1 ">
						<label for="your_unit">Select Unit</label>
						<select name="your_unit" id="your_unit" class="input-text" required>
							<option value="">Unit 1</option>
							<option value="">Unit 2</option>
						</select>
					</div>
					
					<div class="form-row form-row-1 ">
						<label for="your_training">Select Training</label>
						<select type="text" name="your_training" id="your_training" class="input-text form-control" required>
							<option value="">Training 1</option>
							<option value="">Training 2</option>
						</select>
					</div>
				
				<div class="form-checkbox">
					<label class="container"><p>I agree to the Terms and Conditions</p>
					  	<input type="checkbox" name="agree" id="agree">
					  	<span class="checkmark"></span>
					</label>
				</div>
				<div class="form-row-last">
					<input type="button" id="registerBtn" name="register" class="register" value="Register">
				</div>
			</form>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
	<script src="js/iziToast.min.js"></script>
	<script src="js/custom.js"></script>

	<script>
			// just for the demos, avoids form submit
		jQuery.validator.setDefaults({
		  	debug: true,
		  	success:  function(label){
        		label.attr('id', 'valid');
   		 	},
		});
		$( "#myform" ).validate({
		  	
		  	messages: {
		  		first_name: {
		  			required: "Firstname"
		  		},
		  		last_name: {
		  			required: "Lastname"
		  		},
		  		your_email: {
		  			required: "Email"
		  		},
				your_phone: {
		  			required: "Phone"
		  		},
				your_address: {
		  			required: "Address"
		  		},
				your_state: {
		  			required: "State"
		  		}
		  	}
		});
	</script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
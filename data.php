<?php
session_start();
require_once "db.php";
require_once "config.php";
require_once "function.php";
$pref= isset($_REQUEST['ref']) ? htmlspecialchars($_REQUEST['ref']) : "";
//perfectbiller starts here
//$notification_data = $_POST;
/*You can decide to store the notification data to a file a database. */
//error_log(json_encode($notification_data));

$url = "https://api.paystack.co/transaction/verify/$pref";
//die($url);
/*$fields = [
    'email' => "customer@email.com",
    'amount' => "20000"
];
$fields_string = http_build_query($fields);*/
//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
//curl_setopt($ch, CURLOPT_POST, true);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Authorization: Bearer sk_live_024b3053a5198ac3e0c07dc56ac86e364d55862b",
//    "Cache-Control: no-cache",
));

//So that curl_exec returns the contents of the cURL; rather than echoing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

//execute post
$result = curl_exec($ch);

/*$err = curl_error($ch);

if($err){
    print $err;
}*/
curl_close ($ch);


//print($result);


//die();
$response = json_decode($result,true);
if($response['data']['status'] == 'success' /*&& @$response['resp'] == 'Approved' */){
    //you have verified the transaction
    //every other transaction information is stored in the array $response.


    // Optional Block of codes to verify your customer/client
//    $email = $response['data']['customer']['email']; //matches your client's email,
//    $phone = $response['data']['customer']['phone']; //matches your client's email,
    $total = $response['data']['amount']/100; //matches your transaction value,
    //$response['transaction_details']['cur']; //matches your transaction currency,
    $transtid = $response['data']['reference']; //matches your client's order id,

 /*   if(@$response['successful_verifications'] > 1){
        //PROTECTION USING THE SAME TRANSACTION TO GET VALUE TWICE
        echo 'This transaction has been verified before';
        exit();
    }*/

    //make call to table
    $sqlvalidate_query = mysqli_query($conn, "SELECT * FROM ".user." WHERE PRef = '$pref' ");

    if(mysqli_num_rows($sqlvalidate_query)<1){
        echo "Details not found in database";
        print json_encode(array(
            "Reference Number" => $pref,

        ));
        die();
    }else{
        $sqlvalidate_query = mysqli_fetch_array($sqlvalidate_query);

		$_SESSION['completed'] = $sqlvalidate_query['PId'];
		$phone = $sqlvalidate_query['PPhone'];
		$email = $sqlvalidate_query['PEmail'];

			$payStatus = 1;
			$rCode = "GO/".date("Y")."/".ref_id("6");
			$sqlupdate_bal = mysqli_query($conn, "UPDATE ".user." SET PPaymentStatus = '$payStatus', PReservationCode = '$rCode', PPaymentData = '$result' WHERE PRef = '$pref'") or die(mysqli_error($conn));
//        echo "User Details Updated Successfully";
        print json_encode(array(
            "status" => true,
            "Phone Number" => $phone,
            "email" => $email,
            "total" => $total,
        ));

		$subject = 'Training Reservation';
		$send_to = $email;
		$message = "
		Dear ".$sqlvalidate_query['PFirstname'].", Thank you, your training reservation was successful, please note that you will be required to come with your reservation code on the day of your training
		
		<br>
		Reservation Code: ".$rCode." 
		<br>
		Phone Number: ".$phone."
		<br>
		
		<br>
		Thank you 		
		";
		 send_mail($send_to, $message, $subject, $company_name, $company_email, $company_domain, $email_password);


    }

    /*
      Block of codes to set your order status to completed,
      log your transaction and notify your customer.
    */
}else{
    echo json_encode($response);
}
exit();

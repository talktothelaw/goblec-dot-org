
<form id="paymentForm">
    <input type="hidden" id="email-address" value="<?php echo $userdata['PEmail']; ?>" required />
    <input type="hidden" id="amount" value="<?php echo $total; ?>" required />
    <input type="hidden" value="<?php echo $userdata['PFirstname']; ?>" id="first-name" />
    <input type="hidden" id="last-name" value="<?php echo $userdata['PLastname']; ?>"/>
    <button type="submit" class="btn btn-red text-center mt-4" onclick="payWithPaystack()"> Pay </button>

</form>

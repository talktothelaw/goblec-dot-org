<?php
session_start();
require_once "../../db.php";
require_once "../../function.php";
require_once "../../config.php";
require_once "islogged.php";
if(isset($_POST['phone']) AND isset($_POST['code'])){
	
	if(empty($_POST['phone']) AND empty($_POST['code'])){
		print json_encode(array(
				"flaged" => 'error',
				"mess" => "You have submitted an empty input"
			));
	}else{
		
		$phone = mysqli_real_escape_string($conn, $_POST['phone']);
		$code = mysqli_real_escape_string($conn, $_POST['code']);
		
		$qeury = mysqli_query($conn, "select * from ".user." where PPhone = '$phone' AND PReservationCode = '$code' and PPaymentStatus = 1" ) or die(mysqli_error($conn));
		
		if(mysqli_num_rows($qeury) <1){
			print json_encode(array(
				"flaged" => 'error',
				"mess" => "Sorry this reservation code was not found"
			));
		}else{
			
			$reservationData = mysqli_fetch_assoc($qeury);
			
			$t = $reservationData['PTrainingId'];
			$Ptrain = mysqli_query($conn, "select * from ".train." where TId = '$t'") or die(mysqli_error($conn));
			$trainingData = mysqli_fetch_assoc($Ptrain);
			
			$u = $reservationData['PUnitId'];
			$Punit = mysqli_query($conn, "select * from ".unit." where SUId = '$u'") or die(mysqli_error($conn));
			$unitData = mysqli_fetch_assoc($Punit);
			
			if($reservationData['PActivationStatus'] == 0){
				$status_ = '<i class="approved">Not Activated</i>';
				$date = "Not Activated";
				}else if($reservationData['PActivationStatus'] == 1){
				$status_ = '<i class="approved">Activated</i>';
				$date = $reservationData['PActivatedDate'];
			}
			
			print json_encode(array(
				"flaged" => 'success',
				"rmess" => "Congratulation, your reservation was successful!",
				"rcode" => $reservationData['PReservationCode'],
				"rdate" => $reservationData['PRegisteredDate'],
				"fname" => $reservationData['PFirstname'],
				"lname" => $reservationData['PLastname'],
				"activated" => $date,
				"activated_status" => $status_,
				"training" => $trainingData['TName'],
				"runit" => $unitData['SUName'],
				"rtype" => "Training"
			));
		}
		
		
	}
		
	}
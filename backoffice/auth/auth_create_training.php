<?php
session_start();
require_once "../../db.php";
require_once "../../function.php";
require_once "../../config.php";
require_once "islogged.php";

if(isset($_POST['tname'])){
if(empty($_POST['tname']) OR empty($_POST['tunit']) OR empty($_POST['taddress']) OR empty($_POST['tprice']) OR empty($_POST['tstatus']) OR empty($_POST['start'])){
	$_SESSION['msg'] = "<div class='alert alert-danger'>Sorry, you need to enter all details</div>";
	header("location: create_unit.php");
}else{
	
	$tname = mysqli_real_escape_string($conn, $_POST['tname']);
	$tunit = mysqli_real_escape_string($conn, $_POST['tunit']);
	$taddress = mysqli_real_escape_string($conn, $_POST['taddress']);
	$tprice = mysqli_real_escape_string($conn, $_POST['tprice']);
	$tstatus = mysqli_real_escape_string($conn, $_POST['tstatus']);
	$start = mysqli_real_escape_string($conn, $_POST['start']);
	
	$query_it = mysqli_query($conn, "select * from ".train." where TName = '$tname'") or die(mysqli_error($conn));
	
	if(mysqli_num_rows($query_it)<1){
		//insert
		$query_it2 = mysqli_query($conn, "insert into ".train." (TName, TUnit, TVenue, TStatus, TTrainPrice, TStarts) values ('$tname', '$tunit', '$taddress', '$tstatus', '$tprice', '$start') ") or die(mysqli_error($conn));
		if(mysqli_affected_rows($conn)){
			
			$_SESSION['msg'] = "<div class='alert alert-success'>You have successfully created this training</div>";
			header("location: create_training.php");
		}else{
			$_SESSION['msg'] = "<div class='alert alert-danger'>Unable to create this training </div>";
			header("location: create_training.php");

		}
		
	}else{
		
		$_SESSION['msg'] = "<div class='alert alert-danger'>You have already create this training name </div>";
	header("location: create_training.php");
	}
	
	
	
}	
	
}
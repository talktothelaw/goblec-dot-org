<?php
session_start();
require_once "../../db.php";
require_once "../../function.php";
require_once "../../config.php";
require_once "islogged.php";

if(isset($_POST['unit_name'])){
if(empty($_POST['unit_name']) OR empty($_POST['ustatus'])){
	$_SESSION['msg'] = "<div class='alert alert-danger'>Sorry, you need to enter all details</div>";
	header("location: create_unit.php");
}else{
	
	$uname = mysqli_real_escape_string($conn, $_POST['unit_name']);
	$ustatus = mysqli_real_escape_string($conn, $_POST['ustatus']);
	
	$query_it = mysqli_query($conn, "select * from ".unit." where SUName = '$uname'") or die(mysqli_error($conn));
	
	if(mysqli_num_rows($query_it)<1){
		//insert
		$query_it = mysqli_query($conn, "insert into ".unit." (SUName, SUStatus, SUManagerId) values ('$uname', '$ustatus', '1') ") or die(mysqli_error($conn));
		if(mysqli_affected_rows($conn)){
			
			$_SESSION['msg'] = "<div class='alert alert-success'>You have successfully created this unit</div>";
			header("location: create_unit.php");
		}else{
			$_SESSION['msg'] = "<div class='alert alert-danger'>Unable to create this unit </div>";
			header("location: create_unit.php");

		}
		
	}else{
		
		$_SESSION['msg'] = "<div class='alert alert-danger'>You have already create this unit </div>";
	header("location: create_unit.php");
	}
	
	
	
}	
	
}
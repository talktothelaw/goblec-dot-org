<?php
session_start();
require_once "../../db.php";
require_once "../../function.php";
require_once "../../config.php";
require_once "islogged.php";
$query_train = mysqli_query($conn, "select * from ".train."") or die(mysqli_error($conn));
$query_train2 = mysqli_query($conn, "select * from ".train."") or die(mysqli_error($conn));
$query_unit = mysqli_query($conn, "select * from ".unit."") or die(mysqli_error($conn));
if($manager['MLevel'] == "ceo"){
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Ansonika">
  <title>Create Training dashboard</title>
	
  <!-- Favicons-->
  <link rel="shortcut icon" href="../images/icon.png" type="image/x-icon">
  <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">
	
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Main styles -->
  <link href="css/admin.css" rel="stylesheet">
  <!-- Icon fonts-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Plugin styles -->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="vendor/dropzone.css" rel="stylesheet">
  <link href="vendor/fullcalendar.css" rel="stylesheet">
  <link href="vendor/fullcalendar.print.css" rel="stylesheet" media="print">
  <!-- Your custom styles -->
  <link href="css/custom.css" rel="stylesheet">
  <!-- WYSIWYG Editor -->
  <link rel="stylesheet" href="js/editor/summernote-bs4.css">
	
</head>

<body class="fixed-nav sticky-footer" id="page-top">
  <!-- Navigation-->
 <?php include "menu.php"; ?>
  <!-- /Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Create Training</li>
      </ol>
	  
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-file"></i>Training info</h2>
			</div>
			<?php if(isset($_SESSION['msg'])){ echo $_SESSION['msg']; unset($_SESSION['msg']);} ?>
			<form method="post" action="auth_create_training.php">
			<div class="row">
			<div class="col-md-10">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Training Name</label>
						<input type="text" name="tname" class="form-control" placeholder="John Unit">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Select Unit</label>
						<div class="styled-select">
						<select name="tunit" required>
							<option value="">Select</option>
									<?php 
									while($myunitdata = mysqli_fetch_assoc($query_unit)){
									?>	
									<option value="<?php echo $myunitdata['SUId']; ?>"><?php echo $myunitdata['SUName']; ?></option>
									<?php
									}
									?>
						</select>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Training Address</label>
						<textarea type="text" name="taddress" col="6" class="form-control" placeholder="Training Address"></textarea>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Training Start Date</label>
						<input type="date" name="start" class="form-control" placeholder="0.00">
					</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Training Price</label>
						<input type="text" name="tprice" class="form-control" placeholder="0.00">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Select Status</label>
						<div class="styled-select">
						<select name="tstatus">
							<option>Select</option>
							<option value="C">Just Created</option>
							<option value="A">Active</option>
							<option value="P">Passed</option>
							
						</select>
						</div>
					</div>
				</div>
			</div>
			<p><button type="submit" class="btn_1 medium">Save</button></p>
			</div>
			</div>
			</form>
			</div>
		
			
			
			<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-file"></i>Activate Training</h2>
			</div>
			<?php if(isset($_SESSION['msg2'])){ echo $_SESSION['msg2']; unset($_SESSION['msg2']);} ?>
			<div class="col-md-10">
			<form method="post" action="auth_activate_training.php">
				<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					<label>Select Training</label>
						<div class="styled-select">
						<select name="this_training">
							<option value="">Select</option>
							<?php 
							while($trainData = mysqli_fetch_assoc($query_train)){
							?>
							<option value="<?php echo $trainData['TId']; ?>"><?php echo $trainData['TName']; ?></option>
							<?php
							}
							?>
						</select>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
					<label>Select</label>
						<div class="styled-select">
						<select name="tstatus">
							<option value="">Select</option>
							<option value="A">Active</option>
							<option value="P">Passed</option>
														
						</select>
						</div>
					</div>
				</div>
			</div>
			<p><button type="submit" class="btn_1 medium">Activate</button></p>
			</form>
			</div>
			</div>
			


	<!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> List of Training
		  
			<div class="header_box version_2" style="float:right; display:none">
				<button class="btn_1 medium" data-target="#unitupload" data-toggle="modal"><i class="fa fa-upload"></i> Upload</button>
			</div>
		  </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Training Name</th>
                  <th>Status</th>
                  <th>Date</th>
                  <th>Price</th>
                  <th>Created By</th>
                 
                </tr>
              </thead>
             
              <tbody>
			  <?php 
			  while($tdatas = mysqli_fetch_assoc($query_train2)){
			  ?>
                <tr>
                  <td><?php echo $tdatas['TName']; ?></td>
                  <td>
				  <?php if($tdatas['TStatus'] =='A'){ ?>
				  <span class="badge badge-success">Active</span>
				  <?php
					}else if($tdatas['TStatus'] =='C'){
				  ?>
				  <span class="badge badge-primary">Created</span>
				  <?php
					}else if($tdatas['TStatus'] =='P'){
				  ?>
				  <span class="badge badge-danger">Passed</span>
				  <?php 
					}
				  ?>
				  </td>
                  
                  <td><?php echo $tdatas['TDate']; ?></td>
                  <td><?php echo $tdatas['TTrainPrice']; ?></td>
                  
                  <td>Admin</td>
                </tr>
                <?php
				}
				?>
				
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
	  <!-- /tables-->


		</div>
		<!-- /box_general-->
		

		
	  </div>
	  <!-- /.container-fluid-->
   	</div>
    <!-- /.container-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright <?php echo date("Y"); ?></small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
  <?php include "logoutform.php"; ?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <script src="vendor/jquery.selectbox-0.2.js"></script>
    <script src="vendor/retina-replace.min.js"></script>
    <script src="vendor/jquery.magnific-popup.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/admin.js"></script>
    <!-- Custom scripts for this page-->
    <script src="vendor/dropzone.min.js"></script>
    <!-- WYSIWYG Editor -->
    <script src="js/editor/summernote-bs4.min.js"></script>
    <script>
    $('.editor').summernote({
        fontSizes: ['10', '14'],
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']]
        ],
        placeholder: 'Write here ....',
        tabsize: 2,
        height: 200
    });
    </script>
    <!-- SPECIFIC CALENDAR -->
    <script src="vendor/moment.min.js"></script>
    <script src="vendor/jquery-ui.custom.min.js"></script>
    <script src="vendor/fullcalendar.min.js"></script>
    <script src="js/fullcalendar_func.js"></script>
	
</body>
</html>
<?php
}else{
	$_SESSION['msg'] = "<div class='alert alert-danger'>Session Expired</div>";
	header("location: logout.php");
}
?>
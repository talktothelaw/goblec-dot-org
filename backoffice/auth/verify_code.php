<?php
session_start();
require_once "../../db.php";
require_once "../../function.php";
require_once "../../config.php";
require_once "islogged.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Ansonika">
  <title>Verify dashboard</title>
	
  <!-- Favicons-->
  <link rel="shortcut icon" href="../images/icon.png" type="image/x-icon">
  <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">
	
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Main styles -->
  <link href="css/admin.css" rel="stylesheet">
  <!-- Icon fonts-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Plugin styles -->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="vendor/dropzone.css" rel="stylesheet">
  <link href="vendor/fullcalendar.css" rel="stylesheet">
  <link href="vendor/fullcalendar.print.css" rel="stylesheet" media="print">
  <!-- Your custom styles -->
  <link href="css/custom.css" rel="stylesheet">
  <!-- WYSIWYG Editor -->
  <link rel="stylesheet" href="js/editor/summernote-bs4.css">
	
</head>

<body class="fixed-nav sticky-footer" id="page-top">
  <!-- Navigation-->
 <?php include "menu.php"; ?>
  <!-- /Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Verify</li>
      </ol>
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-file"></i>Verify Reservation Code</h2>
			</div>
			<form method="post"> 
			<div class="row">
				
				<div class="col-md-6">
					<div class="form-group">
						<label>Reservation Code</label>
						<input type="text" name="rcode" id="rcode" class="form-control" placeholder="">
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
						<label>Registered Phone Number</label>
						<input type="text" name="phone" id="phone" class="form-control" placeholder="">
					</div>
				</div>
				
				
			</div>
			<p><button type="button" id="verifyCodeNow" class="btn_1 medium">Verify</button></p>
		</form>
			<!-- /row-->
			
			<div class="list_general">
				<center><span style="font-size:50px; display:none" id="loader" class="fa fa-spinner fa-spin"></span></center>
			</div>
			
			<div class="list_general" id="replaceHere">
				
			</div>
			
		
			
		</div>
		<!-- /box_general-->
		

		
	  </div>
	  <!-- /.container-fluid-->
   	</div>
    <!-- /.container-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright <?php echo date("Y"); ?></small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
  <?php include "logoutform.php"; ?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <script src="vendor/jquery.selectbox-0.2.js"></script>
    <script src="vendor/retina-replace.min.js"></script>
    <script src="vendor/jquery.magnific-popup.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/admin.js"></script>
    <!-- Custom scripts for this page-->
    <script src="vendor/dropzone.min.js"></script>
    <!-- WYSIWYG Editor -->
    <script src="js/editor/summernote-bs4.min.js"></script>
    <script>
    $('.editor').summernote({
        fontSizes: ['10', '14'],
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']]
        ],
        placeholder: 'Write here ....',
        tabsize: 2,
        height: 200
    });
    </script>
    <!-- SPECIFIC CALENDAR -->
    <script src="vendor/moment.min.js"></script>
    <script src="vendor/jquery-ui.custom.min.js"></script>
    <script src="vendor/fullcalendar.min.js"></script>
    <script src="js/fullcalendar_func.js"></script>
    <script>
	
	$("#verifyCodeNow").click(function(){
		var code = $("#rcode").val();
		var phone = $("#phone").val();
		$("#loader").show();
		$.ajax({
			url:'check_reservation_code.php',
			method:'post',
			dataType:'JSON',
			data:{code:code, phone:phone},
			success:function(resps){
				$("#loader").hide();
				if(resps.flaged === 'error'){
					$("#replaceHere").html("<h5 class='alert alert-danger'>"+resps.mess+"</h5>")
				}else if(resps.flaged === 'success'){
					$("#replaceHere").html('<div class="list_general"><ul><li><figure><img src="img/avatar.jpg" alt=""></figure><h4><strong id="">'+resps.fname+' '+resps.lname+'</strong> <strong>'+resps.activated_status+'</strong></h4><ul class="booking_list text-bold" style="font-size:15px"><li><strong>Reservation Code:</strong> <strong id="">'+resps.rcode+'</strong></li><li><strong>Reservation Date:</strong> <strong id="">'+resps.rdate+'</strong></li><li><strong>Activation Date:</strong> <strong id="">'+resps.actvated+'</strong></li><li><strong>Type:</strong> <strong id="">'+resps.rtype+'</strong></li><li><strong>Training: </strong> <strong id="">'+resps.training+'</strong></li><li><strong>Unit:</strong> <strong id="">'+resps.runit+'</strong></li></ul><ul class="buttons"><li><a href="active_code.php?active='+resps.rcode+'" class="btn_1 gray approve"><i class="fa fa-fw fa-check-circle-o"></i> Approve</a></li><li><a href="#" onclick="btnCancel()" class="btn_1 gray delete"><i class="fa fa-fw fa-times-circle-o"></i> Cancel</a></li></ul></li></ul></div>');
				}
							
			}
		});
	});
	
	btnCancel = function(){
		location.reload();
	}
	
	</script>
	
</body>
</html>

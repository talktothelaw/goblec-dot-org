<?php
session_start();
require_once "../../db.php";
require_once "../../function.php";
require_once "../../config.php";
require_once "islogged.php";
if(isset($_POST['this_training'])){
if(empty($_POST['this_training']) OR empty($_POST['tstatus'])){
	$_SESSION['msg2'] = "<div class='alert alert-danger'>Sorry, you need to select want you want to activate</div>";
	header("location: create_training.php");
}else{
	
	$this_training = mysqli_real_escape_string($conn, $_POST['this_training']);
	$tstatus = mysqli_real_escape_string($conn, $_POST['tstatus']);
	
	
	$querydb = mysqli_query($conn, "select * from ".train." where TId = '$this_training'") or die(mysqli_error($conn));
	$xdata = mysqli_fetch_assoc($querydb);
	
	if($xdata['TStatus'] == $tstatus AND $xdata['TStatus'] =="C"){
		$_SESSION['msg2'] = "<div class='alert alert-danger'>Sorry, the status of the training is already on Created</div>";
		header("location: create_training.php");
	}else{
		
		if($xdata['TStatus'] == $tstatus  AND $xdata['TStatus'] =="P"){
		$_SESSION['msg2'] = "<div class='alert alert-danger'>Sorry, the status of the training is already on Passed</div>";
		header("location: create_training.php");
	}else{
		
		if($xdata['TStatus'] == $tstatus  AND $xdata['TStatus'] =="A"){
		$_SESSION['msg2'] = "<div class='alert alert-danger'>Sorry, the status of the training is already on Active</div>";
		header("location: create_training.php");
	}else if($xdata['TStatus'] != $tstatus){
	
		$query_it = mysqli_query($conn, "update ".train." set  TStatus = '$tstatus' where TId = '$this_training'") or die(mysqli_error($conn));
		if(mysqli_affected_rows($conn)){	
			$_SESSION['msg2'] = "<div class='alert alert-success'>Status is Updated Successfully</div>";
			header("location: create_training.php");
		}else{
			$_SESSION['msg2'] = "<div class='alert alert-danger'>Unable to update status </div>";
			header("location: create_training.php");

		}
		
	}
		
	}
		
	}
	
	
	
}	
	
}
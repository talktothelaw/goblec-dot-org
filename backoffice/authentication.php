<?php
session_start();
require_once "../db.php";
require_once "../function.php";
require_once "../config.php";
if(isset($_POST['uname']) AND isset($_POST['upass'])){
	
	$uname = mysqli_real_escape_string($conn, $_POST['uname']);
	$upass = mysqli_real_escape_string($conn, $_POST['upass']);
	
	$quser = mysqli_query($conn, "select * from ".mgt." where MEmail = '$uname' OR MPhone = '$uname'") or die(mysqli_error($conn));
	if(mysqli_num_rows($quser)<1){
		$_SESSION['msg'] = "<div class='alert alert-danger'>Invalid Email or Phone</div>";
	header("location: index.php");
	}else{
		
		$hisdata = mysqli_fetch_assoc($quser);
		if(password_verify($upass, $hisdata['MPassword'])){
			//true
			$_SESSION['manager'] = $hisdata['MId'];
			header("location: auth/");
		}else{
			//false
			$_SESSION['msg'] = "<div class='alert alert-danger'>Invalid Password</div>";
			header("location: index.php");
		}
	}	
}else{
	
	$_SESSION['msg'] = "<div class='alert alert-danger'>Session Expired</div>";
	header("location: index.php");
}
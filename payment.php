<?php
require_once "payment_session.php";
$mdata = json_decode($userdata['PMaterials'], true);
$total = $mdata['total'];
?>
<meta charset="utf-8">
<link rel="shortcut icon" href="images/icon.png" type="image/x-icon">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel="stylesheet" href='css/iziToast.min.css'>
<style>
body {
    color: #000;
    overflow-x: hidden;
    height: 100%;
    background-color: #EF5350;
    background-repeat: no-repeat
}

.card {
    padding: 30px 25px 35px 50px;
    border-radius: 30px;
    box-shadow: 0px 4px 8px 0px #B71C1C;
    margin-top: 50px;
    margin-bottom: 50px
}

.border-line {
    border-right: 1px solid #BDBDBD
}

.text-sm {
    font-size: 13px
}

.text-md {
    font-size: 18px
}

.image {
    width: 60px;
    height: 50px
}

::placeholder {
    color: grey;
    opacity: 1
}

:-ms-input-placeholder {
    color: grey
}

::-ms-input-placeholder {
    color: grey
}

input {
    padding: 2px 0px;
    border: none;
    border-bottom: 1px solid lightgrey;
    margin-bottom: 5px;
    margin-top: 2px;
    box-sizing: border-box;
    color: #000;
    font-size: 16px;
    letter-spacing: 1px;
    font-weight: 500
}

input:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border-bottom: 1px solid #EF5350;
    outline-width: 0
}

button:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    outline-width: 0
}

.btn-red {
    background-color: #EF5350;
    color: #fff;
    padding: 8px 25px;
    border-radius: 50px;
    font-size: 18px;
    letter-spacing: 2px;
    border: 2px solid #fff
}

.btn-red:hover {
    box-shadow: 0 0 0 2px #EF5350
}

.btn-red:focus {
    box-shadow: 0 0 0 2px #EF5350 !important
}

.custom-checkbox .custom-control-input:checked~.custom-control-label::before {
    background-color: #EF5350
}

@media screen and (max-width: 575px) {
    .border-line {
        border-right: none;
        border-bottom: 1px solid #EEEEEE
    }
}
</style>
<div class="container-fluid px-1 px-md-2 px-lg-4 py-5 mx-auto">
    <div class="row d-flex justify-content-center">
        <div class="col-xl-7 col-lg-8 col-md-9 col-sm-11">
            <div class="card border-0">
                <div class="row justify-content-center">
                    <h3 class="mb-4">Payment Checkout</h3>
                </div>
                <div class="row">
                    <div class="col-sm-7 border-line pb-3">
                        <div class="form-group">
                            <p class="text-muted text-sm mb-0">Personal Data,</p> <input type="text" name="name" value="<?php echo $userdata['PFirstname'].' '.$userdata['PLastname']; ?>" readonly size="15">
                        </div>
						<div class="form-group">
                            <p class="text-muted text-sm mb-0">Telphone:</p> <input type="text" name="cvv" value="<?php echo $userdata['PPhone']; ?>" readonly size="10">
                        </div>
                        <div class="form-group">
                            <p class="text-muted text-sm mb-0">Your IP Address</p>
                            <div class="row px-3"> <input type="text" name="card-num" value="<?php echo $userdata['PIpaddress']; ?>" readonly size="18" id="cr_no" minlength="19" maxlength="19">
                                <p class="mb-0 ml-3">/</p> <img class="image ml-1" src="images/user.png">
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="text-muted text-sm mb-0">Date</p> <input type="text" name="exp" value="<?php echo date('d M Y h:i:s a'); ?>" readonly size="18" id="exp" minlength="5" maxlength="5">
                        </div>

                        <div class="form-group mb-0">
                            <div class="custom-control custom-control-inline">  <label for="chk1" class=""><strong>Please Note:</strong> Reservation Code is issued after payment is done</label> </div>
                        </div>
                    </div>
                    <div class="col-sm-5 text-sm-center justify-content-center pt-4 pb-4"> <small class="text-sm text-muted">TRN</small>
                        <h5 class="mb-5"><?php echo $userdata['PRef']; ?></h5> <small class="text-sm text-muted">Payment amount</small>
                        <div class="row px-3 justify-content-sm-center">
                            <h2 class=""><span class="text-md font-weight-bold mr-2">&#8358;</span><span class="text-danger"><?php echo $total; ?></span></h2>
                        </div>
						<?php 
						$phone = $userdata['PPhone'];
						$sss  = "SELECT count(*) cnt from exceptionngo where Phone = '$phone'";
						//print $sss;
							$exc = mysqli_query($conn,$sss) or die(mysqli_error($conn));
                    		   if(mysqli_fetch_assoc($exc)['cnt'] > 0){
                    		       print "<div class='alert alert-success'>Your manual payment details has been automatically updated<br/><button class='btn btn-danger' id='continueReservation'>Click here</button><br/> to Reconfirm Payment</div>";
                    		       print "<input id='myphone' type='hidden' value='$phone' />";
                    		        mysqli_query($conn,"UPDATE ".user." SET  `PPaymentStatus` = '1' WHERE PPhone = '$phone' ") or die(mysqli_error($conn));
                    		        
                    		        print "";
                    		        	
                    		    }else{
                    		        include "perfectbiller.php";
                    		    }
						
						
						
						?>

						<a onclick="return confirm('Are you sure you want to cancel', setTimeout(function(){ window.location.href='cancel.php'}, 3000))" class="text-danger" style="cursor:pointer">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="payBtn">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header bg-danger">
        <h4 class="modal-title text-white">Terms and Condition</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       <h4>Heading</h4>
 <p>When you have alot of content inside the modal, a scrollbar is added to the page. See the examples below to understand it:</p>


      </div>

      </div>
  </div>
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
<script src="https://js.paystack.co/v1/inline.js"></script>
<script src='js/iziToast.min.js'></script>
<script src='js/custom.js'></script>

<!--script src="js/payroll.js"></script-->
<script>

    const paymentForm = document.getElementById('paymentForm');
    paymentForm.addEventListener("submit", payWithPaystack, false);
    function payWithPaystack(e) {
        e.preventDefault();
        let handler = PaystackPop.setup({
            key: 'pk_live_5a4a99377b3ba8117c41baeb5ae9a82d3ad45ba1', // Replace with your public key
            split_code:'SPL_lrIdwDNERF',
            email: document.getElementById("email-address").value,
            amount: document.getElementById("amount").value * 100,
            ref: '<?= $userdata['PRef']; ?>',
            // label: "Optional string that replaces customer email"
            onClose: function(){
                //alert('Window closed.');
                	iziToast.info({
			title: 'Info!',
			message: 'Window closed',
			position: 'topRight'
		  });
            },
            callback: function(response){
                $.ajax({
                    url: "./data.php?ref="+response.reference,
                    type: "post",
                    // data: postData,
                    // dataType: "JSON",
                    success: function (data) {
                        location.href="thank-you.php";
                    }
                });
                let message = 'Payment complete! Reference: ' + response.reference + 'You will be redirected Shortly';
                alert(message);



            }
        });
        handler.openIframe();
    }
</script>

<?php
session_start();
require_once "db.php";
require_once "function.php";
require_once "config.php";
	$query_unit = mysqli_query($conn, "select * from ".unit." where SUStatus = 'Y'");
	$query_ngo = mysqli_query($conn, "select * from goblecngo ORDER BY NId asc");
	$query_train = mysqli_query($conn, "select * from ".train." where TStatus = 'A'");
?>
<link rel="stylesheet" href='css/bootstrap.min.css'>
<link rel="stylesheet" href='font-awesome/css/font-awesome.css'>
<link rel="stylesheet" href='css/iziToast.min.css'>
<style>
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Roboto&display=swap');

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: 'Roboto', sans-serif
}

body {
    background-color: #900c3f
}

.container {
    padding: 20px 50px;
    min-height: 450px
}

.fa-yoast {
    color: #ac1f32;
    font-weight: bold
}

.h4 {
    font-family: 'Poppins', sans-serif
}

a {
    color: #333
}

a:hover {
    text-decoration: none;
    color: #444
}

.search {
    background-color: #fdfbfc
}

.search input {
    width: 100px;
    border: none;
    outline: none;
    padding-top: 2px;
    padding-bottom: 2px;
    background-color: #fdfbfc
}

div.btn,
button.btn {
    background-color: #ac1f32;
    color: #eee
}

div.btn:hover,
button.btn:hover {
    background-color: #ac1f32d7
}

.navbar-light .navbar-nav .nav-link {
    color: #333
}

nav {
    float: left
}

#language {
    float: right
}

#language select {
    border: none;
    outline: none
}

.wrapper {
    width: 85%;
    margin: 20px auto;
    box-shadow: 1px 1px 30px 10px rgba(250, 250, 250, 0.8)
}

.h3 {
    padding-top: 40px;
    font-size: 34px
}

label {
    display: block;
    font-size: 0.8rem;
    font-weight: 700
}

input {
    border: none;
    outline: none;
    border-bottom: 2px solid #ddd;
    width: 100%;
    padding-bottom: 10px
}

.wrapper {
    clear: both
}

#country {
    border: none;
    outline: none;
    width: 100%;
    padding-bottom: 12px;
    border-bottom: 2px solid #ddd
}

.wrapper .col-md-6:hover label {
    color: #ac1f32
}

.wrapper .col-md-6:hover input,
.wrapper .col-md-6:hover #country {
    border-color: #ac1f32;
    cursor: pointer
}

.wrapper .col-md-6 input:focus {
    border-color: #ac1f32
}

.option {
    position: relative;
    padding-left: 30px;
    display: block;
    cursor: pointer;
    color: #888
}

.option input {
    display: none
}

.checkmark {
    position: absolute;
    top: -1px;
    left: 0;
    height: 20px;
    width: 20px;
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 50%
}

.option input:checked~.checkmark:after {
    display: block
}

.option .checkmark:after {
    content: "\2713";
    width: 10px;
    height: 10px;
    display: block;
    position: absolute;
    top: 30%;
    left: 50%;
    transform: translate(-50%, -50%) scale(0);
    transition: 200ms ease-in-out 0s
}

.option:hover input[type="radio"]~.checkmark {
    background-color: #f4f4f4
}

.option input[type="radio"]:checked~.checkmark {
    background: #ac1f32;
    color: #fff;
    transition: 300ms ease-in-out 0s
}

.option input[type="radio"]:checked~.checkmark:after {
    transform: translate(-50%, -50%) scale(1);
    color: #fff
}

.option:hover input[type="checkbox"]~.checkmark {
    background-color: #f4f4f4
}

.option input[type="checkbox"]:checked~.checkmark {
    background: #ac1f32;
    color: #fff;
    transition: 300ms ease-in-out 0s
}

.option input[type="checkbox"]:checked~.checkmark:after {
    transform: translate(-50%, -50%) scale(1);
    color: #fff
}

@media (min-width: 992px) {
    .navbar-expand-lg .navbar-nav .nav-link {
        padding-right: 1.5rem;
        padding-left: 0rem
    }

    .navbar {
        padding: 0.5rem 0rem;
        width: 75%
    }
}

@media(max-width: 991px) {
    .dropdown-menu {
        border: none
    }

    #language {
        padding-top: 20px
    }

    .navbar {
        padding: 0.5rem 0rem
    }
}

@media(max-width: 767px) {
    .search input {
        width: 90%
    }

    .search {
        margin-bottom: 10px
    }

    div.btn {
        width: 100%
    }

    .h3 {
        font-size: 25px
    }

    .brand {
        text-align: center
    }

    .container {
        padding: 20px;
        margin-left: 0px
    }

    .navbar {
        padding: 0.5rem 0rem
    }
}

@media(max-width: 374px) {
    .h3 {
        font-size: 21px
    }
}
</style>
<title>Training Reservation</title>
<meta charset="utf-8">
<link rel="shortcut icon" href="images/icon.png" type="image/x-icon">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">

<div class="container bg-white mt-sm-4 mb-5">
    <div class="d-md-flex flex-md-row">
        <div class="brand text-uppercase h4 font-weight-bold"> <a href="#"><img src="images/logo.png"/>GOLDEN BRIDGE LIFE ENHANCEMENT CENTER</a> </div>
        <div class="ml-auto px-2 pt-1 rounded">
			<img src="images/flag-ng.png" style="width:30px"/> Nigeria
		</div>
        <div class="btn px-4 ml-md-3" data-toggle="modal" data-target="#mycontinue">Continue Reservation</div>
    </div>
    <div class="d-lg-flex align-items-center">
        <nav class="navbar navbar-expand-lg navbar-light bg-white"> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
			Your IP Address: <strong><?php echo get_ip(); ?></strong> is logged for fraud prevention
                <!--ul class="navbar-nav">
                    <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Products </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> <a class="dropdown-item" href="#">Storage Security</a> <a class="dropdown-item" href="#">Secure Gateway</a> <a class="dropdown-item" href="#">All Products</a> </div>
                    </li>
                    <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Partners </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> <a class="dropdown-item" href="#">Security Partners</a> <a class="dropdown-item" href="#">Channel Partners</a> </div>
                    </li>
                    <li class="nav-item"> <a class="nav-link" href="#">Support</a> </li>
                    <li class="nav-item"> <a class="nav-link" href="#">Resources</a> </li>
                    <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Company </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> <a class="dropdown-item" href="#">News</a> <a class="dropdown-item" href="#">About Us</a> </div>
                    </li>
                    <li class="nav-item"> <a class="nav-link" href="#">Contact us</a> </li>
                </ul-->
            </div>
        </nav>
        <div class="text-muted ml-auto" id="language">
		<?php echo date("d M Y"); ?>
			</div>
    </div>
 

	<form autocomplete="off" method="post">
    <div class="wrapper d-flex justify-content-center flex-column px-md-5 px-1">
	<p class="text-center"><?php if(isset($_SESSION['msg'])){echo $_SESSION['msg']; unset($_SESSION['msg']);} ?></p>
	
	
	 <div class="alert" style="background:url('smile.jpg');background-size:cover;font-size:italic;font-size:small;box-shadow: 0px 0px 3px;">
        <!--
        <b>Golden Bridge</b> is partnering with NGOs and Cooperatives to accelerate United Nation's SDG No. 1 POVERTY ALLEVIATION. 
        <br/>

Through Group Capacity Building, Microfinance and Financial Inclusion, we help our partners train and empower their members with grants, loans and other financial instruments.
<br/>
The SDG2020 Grant Program is an ongoing MSME Development Grant program launched in 2020. It's targeted at providing Pilot and Scale-up  Projects with grants for Social Enterprise Development through participating NGOs/Coops.

<hr/>

<b>DONATE N50,000 ($100) or above </b>to become our Corporate Partner (NGOs only).
<hr/>
<b>Be CERTIFIED </b>to volunteer as an SDG2020 Unit Manager to a Corporate Partner. The training Package includes a Certificate in Basic Microfinance and Financial Inclusion Services, Resource Materials, a Prepaid ATM Card and a Pre-loaded Mobile CUG Sim Card).

<b>Training Fee - N5,100 ($10) only.</b> 
        <hr/>
       <b> DONATE N8,000 ($15) or above</b> to become a Non-entity Partner (Units of Partner NGOs only).
        
        <hr/>
      <b>Be CERTIFIED</b>   to volunteer as an SDG2020 Unit Manager to our Corporate Partners for N5,100 ($10) only.
<hr/>
The SDG2029 UM Training fees cover a Certificate in Basic Microfinance and Financial Inclusion Services, Resource Materials, a Prepaid ATM Card, a Pre-loaded Mobile CUG Sim Card and a one-time Service Logistics Fee.
  -->      
    
	<center><b style="font-size:20px">SDG-2020 CALL FOR PROPOSALS </b></center>

<p><b>Golden Bridge Life Enhancement Centre (Goblec)</b> is partnering CAC incorporated NGOs to accelerate UN Sustainable Development Goals through <b>POVERTY ALLEVIATION.</b> </p>

<p>Through <b>Group Capacity Building</b>, Microfinance and Financial Inclusion, we empower our Partners with grants and on-lending facilities for their beneficiary groups of 1,000 persons per unit.</p>

<p>This <b>MSME</b> Social Enterprise Development program was launched in 2020 to support local Pilot and Scale-up projects against the effects of the Covid-19 Pandemic and other social hazzards. </p>

<p>The program engages the services of GTC Certified Basic Microfinance and Financial Inclusion Managers to work as <b>VOLUNTEERS in accredited NGO units</b>.</p>

 <hr/>
<b>APPLICATION METHOD</b>

To qualify, an NGO must submit its:
<ol>
	<li>CAC Registration Documents</li>
	<li>SCUML Certificate</li>
	<li>Goblec Partnership Certificate</li>
	<li>Beneficiary Units</li>
	<li>Volunteer Project Managers (3 per Unit)</li>
	<li>Non-Profit Business Proposal (Bankable).</li>
	<li>Audited Financial Statements</li>
	<li>Annual Returns Certificate (1 Year).</li>
</ol>
	
	
	
	</div>
		<div class="h3 text-center font-weight-bold">BULK RESERVATION</div><br>
	<div class="btn px-4 ml-md-3 text-center" data-toggle="modal" data-target="#bulk">CLICK HERE (BULK RESERVATION)</div>
                                    	<br>
                                    	<span class="text-center">OR</span>
                                    
	<div class="h3 text-center font-weight-bold">SINGLE RESERVATION COMPLETE THE FORM BELOW</div>
	
        <div class="h3 text-center font-weight-bold">RESERVATION FOR GTC TRAINING / CERTIFICATION</div>
        <div class="row my-4">
            <div class="col-md-6"> <label>First Name</label>
			<input id="firstname" type="text" placeholder="John "> </div>

            <div class="col-md-6 pt-md-0 pt-4"> <label>Last Name</label>
			<input type="text" placeholder="Smith" id="lastname"> </div>
        </div>
        <div class="row my-md-4 my-2">
            <div class="col-md-6"> <label>Mail</label>
			<input type="email" id="email" placeholder="johnsmith@gmail.com">
			</div>
            <div class="col-md-6 pt-md-0 pt-4"> <label>Phone</label>
			<input type="tel" id="phone" placeholder="070XXXXXXXX">
			</div>
        </div>
        <div class="row my-md-4 my-2">
            <div class="col-md-6"> <label>Address</label>
			<input type="text" id="address" placeholder="Ikeja Lagos">
			</div>


				 <div class="col-md-6 pt-md-0 pt-4"> <label>Select State</label>
				 <select name="state" id="state" class="form-control" required>
                    <option value="">Select</option>
				<option>Outside Nigeria</option>
				<option>ABUJA FCT</option>
				<option>ABIA</option>
				<option>ADAMAWA</option>
				<option>AKWA IBOM</option>
				<option>ANAMBRA</option>
				<option>BAUCHI</option>
				<option>BAYELSA</option>
				<option>BENUE</option>
				<option>BORNO</option>
				<option>CROSS RIVER</option>
				<option>DELTA</option>
				<option>EBONYI</option>
				<option>EDO</option>
				<option>EKITI</option>
				<option>ENUGU</option>
				<option>GOMBE</option>
				<option>IMO</option>
				<option>JIGAWA</option>
				<option>KADUNA</option>
				<option>KANO</option>
				<option>KATSINA</option>
				<option>KEBBI</option>
				<option>KOGI</option>
				<option>KWARA</option>
				<option>LAGOS</option>
				<option>NASSARAWA</option>
				<option>NIGER</option>
				<option>OGUN</option>
				<option>ONDO</option>
				<option>OSUN</option>
				<option>OYO</option>
				<option>PLATEAU</option>
				<option>RIVERS</option>
				<option>SOKOTO</option>
				<option>TARABA</option>
				<option>YOBE</option>
				<option>ZAMFARA</option>
                </select>
				</div>
        </div>

		<div class="row my-md-4 my-2">
            <div class="col-md-6 pt-md-0 pt-4"> <label>Select Unit</label>
			<select name="unit" id="unit" class="form-control" required>
                    <option value="">Select</option>
                    <?php
					while($unitdata = mysqli_fetch_assoc($query_unit)){
					?>
					<option value="<?php echo $unitdata['SUId']; ?>"><?php echo $unitdata['SUName']; ?></option>
                    <?php
					}

					?>
                </select>
				</div>
            <div class="col-md-6 pt-md-0 pt-4"> <label>Select NGO</label>
			<select name="ngo" id="ngo" class="form-control" required>
                    <option value="">Select</option>
                    <?php
					while($ngodata = mysqli_fetch_assoc($query_ngo)){
					?>
					<option value="<?php echo $ngodata['NId']; ?>"><?php echo $ngodata['NGO']; ?></option>
                    <?php
					}

					?>
                </select>
				</div>
        </div><div class="row my-md-4 my-2">
            <div class="col-md-6 pt-md-0 pt-4"> <label>Select Training</label>
			<select name="training" id="training" required class="form-control" onchange="getmaterials(this.value)">
                    <option value="">Select</option>
                    <?php
                    $mytdata = [];
					while($tdata = mysqli_fetch_assoc($query_train)){
					    $tchunk = array(
					            "name" => $tdata['TName'],
					            "price" => $tdata['TTrainPrice'],
					            "id" => $tdata['TId'],
                        );
					    $mytdata[$tdata['TId']] = $tchunk;
					?>
					<option value="<?php echo $tdata['TId']; ?>"><?php echo $tdata['TName'].' => Price:'.$tdata['TTrainPrice']; ?></option>
                    <?php
					}

					?>
                </select>
				</div>

        </div>

        <div id="X_Y"></div>



		<hr>

		<div class="d-lg-flex justify-content-between align-items-center pb-4">
            <div class="size"> <label class="option">
			<input type="radio" name="agree"> I have read and understand the terms and condition <span class="checkmark"></span> </label> </div>

        </div>

        <div class="alert alert-danger" style="font-weight: bold">
            NGN <span id="priceplace">0.00</span>
        </div>
        <div class="d-flex justify-content-end">

		<button type="button" class="btn btn-move">Proceed with Reservation</button>
		</div>
    </div>
	</form>

</div>


<div class="modal fade" id="mycontinue">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header bg-danger">
        <h4 class="modal-title text-white">Continue Reservation</h4>
        <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form autocomplete="off">
			<div class="form-group">
				<label>Enter your phone Number</label>
				<input type="text" name="myphone" autofocus id="myphone" placeholder="07065326162" class="form-control"/>
			</div>

			<div class="form-group">
				<input type="button" id="continueReservation" class="btn btn-danger" value="Continue"/>
			</div>
		</form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="bulk">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header bg-danger">
        <h4 class="modal-title text-white">Bulk Reservation</h4>
        <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form autocomplete="off" action="multiple.php" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label>Excel file</label>
				<input type="file" name="excel" accept=".xlsx, .xls, .csv" autofocus id="excel" class="form-control"/>
			</div>

<div class="form-group">
				<label>Uploader Email</label>
				<input type="email" name="uploadermail" id="uploadermail" placeholder="Email@yahoo.com" class="form-control" required/>
			</div>
			
			<div class="form-group">
				<label>Uploader Name</label>
				<input type="text" name="uploadername" id="uploadername" placeholder="John Doe" class="form-control" required/>
			</div>
			
			
			<div class="form-group">
				<input type="submit" id="bulkuploadbtn" name="bulkuploadbtn" class="btn btn-danger" value="Upload"/>
			</div>
			<div class="form-group">
			<a class="text-center" href="template.xlsx">Click Here to Download Excel Template format</a>
			</div>
			<div class="form-group">
			<small><code class="">Please note the following:</code></small>
			<small>
			   <ol style="margin-left:10px">
			    <li>You are required to remove the example data inside the template.</li>
			     <li>Kindly make sure that the Excel file is in Micro soft Excel format.</li>
			     <li>Make sure that you have a stable network.</li>
			     <li>Split an Excel that is above 3Mb.</li>
			</ul>
			</small>
			</div>
			
			
		</form>
      </div>
    </div>
  </div>
</div>


<script src='js/jquery.min.js'></script>
<script src='js/bootstrap.bundle.min.js'></script>
<script src='js/iziToast.min.js'></script>
<script src='js/custom.js'></script>
<?php
print "<script>
var tdata = ".json_encode($mytdata).";
</script>"
?>
<script>
    var totalprice = 0;
    function getmaterials(x){
        totalprice = 0;
        $.ajax({
            url:'fetch_materials.php',
            method:'post',
            dataType:'html', data:{T:x},
            success:function(respdata){
                $("#X_Y").html(respdata);
            }
        });
        let trainprice = tdata[x].price
        totalprice += parseFloat(trainprice);
        $("#priceplace").text(totalprice)
    }
    var training_materials = [];
    function calcMaterials(cb){
        let thismaterial = tmaterials[cb.value];
        // console.log(thismaterial);
            if(cb.checked){
                totalprice += parseFloat(thismaterial.price);
                training_materials[thismaterial.id] = thismaterial;
            }else{
                totalprice -= parseFloat(thismaterial.price);
                delete(training_materials[thismaterial.id]);
            }
        $("#priceplace").text(totalprice)
    }
    
    /*$("#bulkuploadbtn").click(function(){
        var excel = $("#excel").val();
        var uploadermail = $("#uploadermail").val();
        var excel = $("#uploadername").val();
        
    })*/

</script>

<?php
session_start();
require_once "db.php";
require_once "function.php";
require_once "config.php";
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;

if(isset($_POST["bulkuploadbtn"])){

  $inputFileName = $_FILES["excel"]["name"];
  $fileSize=$_FILES["excel"]["size"]/1024;
  $fileType=$_FILES["excel"]["type"];
  $fileTmpName=$_FILES["excel"]["tmp_name"];  

      //File upload path
      $uploadPath="uploads/".$inputFileName;

      //function for upload file
      if(move_uploaded_file($fileTmpName,$uploadPath)){
        

$spreadsheet = IOFactory::load($uploadPath);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
//print json_encode($sheetData);



function regSingle($rowId,$singleData,$training,$fee){
//    print(json_encode($singleData));


   global $conn;

    $fname = mysqli_real_escape_string($conn, $singleData['B']);
    $lname = mysqli_real_escape_string($conn, $singleData['C']);
    $mail = mysqli_real_escape_string($conn, $singleData['D']);
    $addr = mysqli_real_escape_string($conn, $singleData['F']);
    $phone = mysqli_real_escape_string($conn, $singleData['E']);
    $state = mysqli_real_escape_string($conn, $singleData['G']);
    $train = mysqli_real_escape_string($conn, $training);
    $unit = mysqli_real_escape_string($conn, $singleData['H']);
    $ngo = mysqli_real_escape_string($conn, $singleData['I']);
    
    //real ngo
    $ngoq = mysqli_query($conn, "select NId FROM goblecngo WHERE CODE = '$ngo'") or die(mysqli_error($conn));
    if(mysqli_num_rows($ngoq) > 0){
        $ngo = mysqli_fetch_assoc($ngoq)['NId'];
    }else{
        return(json_encode(array(
            "flag" => 0,
            "msg" => "Error on row $rowId: $ngo is not a valid NGO CODE "
        )));
    }

    //end real ngo
    
    $_tdata =  [];
    $referenceId = ref_id('3').'-'.ref_id('3').'-'.ref_id('3');
    $ip = get_ip();

    //get training price
    $query_train = mysqli_query($conn, "select * from ".train." where TStatus = 'A'");
    $tdata = mysqli_fetch_assoc($query_train);
    $amount = $tdata['TTrainPrice'];

    if(!filter_var($mail, FILTER_VALIDATE_EMAIL)){
        return(json_encode(array(
            "flag" => 0,
            "msg" => "Error on row $rowId: $mail is not a valid email "
        )));
    }else{
        $qq = "select * from ".user." where PEmail = '$mail'";
//        die($qq);
        $query = mysqli_query($conn, $qq) or die(mysqli_error($conn));
//		die(json_encode(mysqli_fetch_array($query)));
        if(mysqli_num_rows($query)>0){
            return(json_encode(array(
                "flag" => 0,
                "msg" => "Error on row $rowId: $mail is already registered "
            )));
        }else{

            $query = mysqli_query($conn, "select * from ".user." where PPhone = '$phone'") or die(mysqli_error($conn));
            if(mysqli_num_rows($query)>0){
                return(json_encode(array(
                    "flag" => 0,
                    "msg" => "Error on row $rowId: $phone is not already registered "
                )));
            }else{
                //verifying no manipulation


               /* $_rdata = $_tdata;
                if(gettype($_rdata) != "array"){
                    return(json_encode(array(
                        "flag" => 0,
                        "msg" => "Unable to read data from training materials selected "
                    )));
                }else{
                    foreach($_rdata['materials'] as $_rd){
//                        die(json_encode($_rdata));
                        $ids = 0;
                        if($_rd != null){
                            $ids .= is_numeric($_rd['id']) ? ",".$_rd['id'] : 0;
                            //check final manipulation
                        }
                    }
                    $getit = mysqli_query($conn,"SELECT * FROM  ".material." where TMid in ($ids) ") or die(mysqli_error($conn));
                    while($k = mysqli_fetch_assoc($getit)){
                        if($_rdata['materials'][$k['TMid']]['price'] == $k['TMprice']){
                            $amount += $_rd['price'];
                        }else{
                            return(json_encode(array(
                                "flag" => 0,
                                "msg" => "Unable to resolve calculation of training materials "
                            )));
                        }

                    }
                }*/


global $refn;
                //end of verifying no manipulation
                $_tdataS = json_encode($_tdata);
                $qslupdate = mysqli_query($conn, "insert into bulkparticipants (`PFirstname`, `PLastname`,  `PPhone`, `PState`, `pAddress`, `PEmail`, `PTrainingId`, `PUnitId`, `PRef`, `PIpaddress`, `PAmount`,`PMaterials`,`PNGO`,`PBulkRef`) values ('$fname', '$lname', '$phone', '$state', '$addr', '$mail', '$train', '$unit', '$referenceId', '$ip', '$amount','$_tdataS','$ngo','$refn') ") or die(mysqli_error($conn));

                if(mysqli_affected_rows($conn)<1){
                    return(json_encode(array(
                        "flag" => 0,
                        "msg" => "Unable to create account from row $rowId downwards "
                    )));
                }else{

                    $insertID = mysqli_insert_id($conn);
                    $_SESSION['hisdata'] = $insertID;
                    return(json_encode(array(
                        "flag" => 1,
                        "msg" => "Success "
                    )));

                }

            }

        }

    }




}

if($sheetData[1] != array(
    "A" => "SN",
    "B" => "FIRSTNAME",
    "C" => "LASTNAME",
    "D" => "MAIL",
    "E" => "PHONE",
    "F" => "ADDRESS",
    "G" => "STATE",
    "H" => "UNIT",
    "I" => "NGO"
    )){

   $_SESSION["msg"] = "<div class='alert alert-danger'>Incorrect Excel Template, Please make sure your columns are well formatted</div>";
   header("location: index.php");
   die();

}
$sn = 0;
$fee = 5303;
$total = 0;
$training = 1;

if(!isset($_REQUEST['uploadermail']) && !isset($_REQUEST['uploadername'])){
   
        
        $_SESSION["msg"] = "<div class='alert alert-danger'>Uploader Name and email are required</div>";
        header("location: index.php");
        die();
  
}



$refn = uniqid("GOB-");
foreach($sheetData as $vv){
    if($sn>0){
        $cc = regSingle($sn,$vv,$training,5303);
        $cc = json_decode($cc,true);
        if(!$cc['flag']){
            
        $_SESSION["msg"] = "<div class='alert alert-danger'>".$cc['msg']."</div>";
        header("location: index.php");
        die();
            
        }
    }
    $total += $fee;
    $sn++;
}
//create bulk ref
$uploadername = mysqli_real_escape_string($conn,$_REQUEST['uploadername']);
$uploadermail = mysqli_real_escape_string($conn,$_REQUEST['uploadermail']);
$c = mysqli_query($conn,"INSERT INTO bulkreg (BRRef,BRAmount,BRStatus,BRCreated,BRPaid,BRPayer,BRPEmail) VALUES ('$refn','$total','N',NOW(),NULL,'$uploadername','$uploadermail')") or die(mysqli_error($conn));


$_SESSION["msg"] = "<div class='alert alert-success'>".($sn-1)." records created with ref number $refn, amount to pay $total";
        header("location: paybulk.php?bulkid=".$refn);
        die();


      }
    
}

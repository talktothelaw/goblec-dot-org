<?php
session_start();
require_once "db.php";
require_once "function.php";
require_once "config.php";
if(isset($_SESSION['completed'])){
	$sid = mysqli_real_escape_string($conn, $_SESSION['completed']); 	
$query = mysqli_query($conn, "select * from ".user." where PId = '$sid'") or die(mysqli_error($conn));
if(mysqli_num_rows($query)){
	$client = mysqli_fetch_assoc($query);
	
?>

<link rel="stylesheet" href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css'>
<style>
body {
    background-color: #f7f6f6
}

.card {
    width: 800px
}

.icon i {
    font-size: 25px;
    color: #2196F3
}

label.radio {
    cursor: pointer;
    width: 100% !important;
    margin-top: 9px
}

label.radio input {
    position: absolute;
    top: 0;
    left: 0;
    visibility: hidden;
    pointer-events: none;
    width: 100%
}

label.radio span {
    padding: 7px 14px;
    border: 1px solid #D32F2F;
    display: inline-block;
    color: #D32F2F;
    border-radius: 3px;
    box-shadow: 3px 5px 8px 2px #e9ecef;
    width: 100%;
    align-items: center
}

label.radio input:checked+span {
    border-color: #D32F2F;
    background-color: #D32F2F;
    color: #fff
}

.area {
    resize: none
}

.area:focus {
    box-shadow: none;
    border-color: #D32F2F !important
}

.submit-button,
.submit-button:active,
.submit-button:visited,
.submit-button:focus {
    background-color: #D32F2F !important;
    border-color: #D32F2F !important;
    color: #fff !important;
    box-shadow: none;
    text-transform: uppercase;
    padding-left: 35px;
    padding-right: 35px
}

.submit-button:hover {
    background-color: #D32F2F !important;
    border-color: #D32F2F !important
}
</style>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	
<div class="container mt-5 mb-5 d-flex justify-content-center ">
    <div class="card">
        <div class="p-3">
		<button onclick="printDiv()" class="btn rounded border shadow">Print</button>
            <div class="first text-center" id="myDiv"> 
				<img src="https://i.imgur.com/KCcF6WN.png" width="80">
                <h3 class="mt-2">Reservation Completed</h3>
                <h4 class="text-black-50">Thanks for your reservation, you will be ask to provide your reservation code which is <strong><?php echo $client['PReservationCode']; ?></strong></h4>
            </div>
			<br>
			<p class="text-black-50 text-center">If you have not uploaded your passport photography please click the button below!</p>
			<div class="button mt-4 text-center"> <a href="capture.php" class="btn btn-success submit-button">Upload Passport</a></div>
			<div class="button mt-4 text-center"> <a href="cart.php" class="btn btn-success submit-button">View/Download certificates</a></div>
			<br>
			<br>
			<br>
			<p class="text-black-50 text-center">Will you like to share your experience, while using our system?</p>
            <div class="border p-3 rounded">
			<form method="post" action="experience.php">
                <div class="row">
                    <div class="col-md-3"> <label class="radio"> <input type="radio" name="experience" value="Bad Experience"> <span>Bad Experience</span> </label> </div>
                    <div class="col-md-3"> <label class="radio"> <input type="radio" name="experience" value="Good Experience"> <span>Good Experience</span> </label> </div>
                    <div class="col-md-3"> <label class="radio"> <input type="radio" name="experience" value="Great Experience" checked> <span>Great Experience</span> </label> </div>
                    <div class="col-md-3"> <label class="radio"> <input type="radio" name="experience" value="Amazing Experience"> <span>Amzng Experience</span> </label> </div>
                </div>
				<input type="hidden" name="subject" value="Customer Feedback Response"/>
                <div class="mt-4"> <textarea class="area form-control" name="experience_message" rows="7" placeholder="add comments"></textarea> </div>
                <div class="button mt-4 text-right"> <button class="btn btn-success submit-button">Submit</button> </div>
				
				</form>
            </div>
        </div>
    </div>
</div>
<script src='js/jquery.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js'></script>


<script type="text/javascript">
        function printDiv(){
		window.print();
    }
    </script>
<?php

//unset($_SESSION['completed']);
}else{
	$_SESSION['msg'] = "<div class='alert alert-danger'>Session has Expired</div>";
	header("location: index.php");	
}

}else{
	//unset($_SESSION['completed']);
	$_SESSION['msg'] = "<div class='alert alert-danger'>Session has Expired</div>";
	header("location: index.php");	
}

?>